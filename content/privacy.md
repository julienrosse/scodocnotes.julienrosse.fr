---
title: "Politique de confidentialité"
date: 2023-05-28T23:14:07+02:00
draft: false
---

## Politique de confidentialité de Scodoc Notes, par Julien ROSSE
Dernière mise à jour : 28/05/2023

La présente politique de confidentialité régit la manière dont nous collectons, utilisons, divulguons
et stockons les informations personnelles des utilisateurs de notre application mobile conformément
au Règlement général sur la protection des données (RGPD). Votre utilisation de notre application
mobile implique votre consentement à la collecte et à l’utilisation de vos informations personnelles
comme décrit dans cette politique de confidentialité.
1. Collecte d’informations

Nous collectons certaines informations personnelles lorsque vous utilisez notre application mobile.
Les types d’informations que nous collectons sont les suivants :

a. Informations d’utilisation : Nous collectons des informations sur la manière dont vous utilisez
notre application mobile, telles que les fonctionnalités utilisées, les actions effectuées et les
préférences sélectionnées. Ces informations sont collectées de manière anonyme et ne permettent
pas d’identifier directement un utilisateur spécifique.

b. Rapports de crashs : Lorsque notre application mobile rencontre un dysfonctionnement ou un
crash, nous pouvons collecter des informations techniques liées à cet incident, telles que les
journaux d’erreurs, les traces de pile d’exécution et les configurations système. Ces informations
sont essentielles pour diagnostiquer et résoudre les problèmes techniques et améliorer la stabilité de
notre application. Les utilisateurs ont le choix d’envoyer ces rapports de crashs par email.

c. Université des utilisateurs : Lors de l’authentification, le nom de l’université à laquelle
l’utilisateur se connecte nous est transmis. Nous pouvons collecter et stocker ces informations de
manière anonyme pour des analyses statistiques et des améliorations de nos services. Ces
informations ne permettent pas d’identifier directement un utilisateur spécifique.

2. Utilisation des informations

Nous utilisons les informations collectées uniquement aux fins suivantes :
    a. Fournir et améliorer notre application mobile : Nous utilisons les informations pour exploiter,
    maintenir et améliorer les fonctionnalités et les performances de notre application mobile, ainsi que
    pour personnaliser votre expérience utilisateur.
    b. Diagnostiquer les problèmes techniques : Les rapports de crashs et les informations techniques
    associées sont utilisés pour diagnostiquer et résoudre les problèmes techniques de notre application
    mobile.
    c. Analyse statistique : Les informations sur l’utilisation de notre application, y compris les
    informations facultatives sur l’université des utilisateurs, peuvent être utilisées de manière agrégée
    et anonyme à des fins d’analyse statistique afin de comprendre les tendances d’utilisation et
    d’améliorer nos services.
3. Divulgation des informations

Nous ne divulguons pas vos informations personnelles à des tiers, sauf dans les circonstances
suivantes :
    a. Consentement : Nous pouvons partager vos informations personnelles avec votre consentement
    exprès.
    b. Fournisseurs de services : Nous pouvons partager vos informations personnelles avec des
    prestataires de services tiers qui nous aident à fournir, maintenir ou améliorer notre application
    mobile. Ces prestataires de services sont soumis à des obligations de confidentialité et ne sont
    autorisés à utiliser vos informations personnelles que dans la mesure nécessaire pour fournir les
    services pour lesquels ils ont été engagés. Nous utilisons PostHog pour récolter et stocker ces
    données.
    c. Conformité légale : Nous pouvons divulguer vos informations personnelles si nous sommes
    légalement tenus de le faire, notamment pour répondre à des demandes gouvernementales, faire
    respecter nos droits légaux ou protéger la sécurité ou les droits d’autrui.
4. Stockage des informations

Les informations transmises sont sauvegardées sur les serveurs européens de PostHog.
5. Vos droits en matière de protection des données

Conformément au RGPD, vous disposez de certains droits relatifs à vos informations personnelles.
Vous avez le droit de demander l’accès, la rectification, la suppression ou la limitation du traitement
de vos informations personnelles. Vous avez également le droit de vous opposer au traitement de
vos informations personnelles et le droit à la portabilité des données. Pour exercer ces droits,
veuillez nous contacter à l’adresse électronique mentionnée ci-dessous.
6. Modifications de la politique de confidentialité
Nous nous réservons le droit de modifier cette politique de confidentialité à tout moment. Toute
modification sera publiée sur cette page avec la date de la dernière mise à jour. Votre utilisation
continue de notre application mobile après de telles modifications constitue votre consentement à
ces modifications.
7. Nous contacter
Si vous avez des questions, des préoccupations ou des demandes concernant cette politique de
confidentialité, veuillez nous contacter à l’adresse électronique suivante : [julien@julienrosse.fr].

Note du développeur (Julien ROSSE) : Je suis un étudiant en informatique n’ayant aucune
affiliation avec des universités dans le cadre de ce projet. L’application Scodoc Notes a été réalisée
dans l’objectif d’améliorer l’utilisabilité de la plateforme. Je n’ai pas de connaissances particulières
en droit et cette politique de confidentialité a été majoritairement rédigée par ChatGPT dans l'objectif d'informer les utilisateurs de l'application.
